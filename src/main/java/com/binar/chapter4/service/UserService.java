package com.binar.chapter4.service;

import com.binar.chapter4.model.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {
    User getUserByUsername(String username);
    User addUser(User user);
    List<User> getAllUsers();

    void updateUser(String username, User user);
    void deleteUserByUsername(String username);
}
