package com.binar.chapter4.controller;

import com.binar.chapter4.implementation.FilmServiceImpl;
import com.binar.chapter4.model.Film;
import com.binar.chapter4.model.Schedule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/films")
public class FilmController {

    @Autowired
    FilmServiceImpl service;

    @GetMapping("/")
    public Collection<Film> findAll() {
        return service.getAllFilms();
    }

    @GetMapping("/showing")
    public Collection<Film> findShowingFilms() {
        return service.getShowingFilms();
    }

    @PostMapping(value = "/", consumes = {"application/json"})
    @ResponseStatus(HttpStatus.OK)
    public Film storeFilm(@RequestBody final Film Film){
        return service.addFilm(Film);
    }

    @PutMapping(value = "/{film_code}", consumes = {"*/*"})
    @ResponseStatus(HttpStatus.OK)
    public Film updateFilm(@PathVariable("film_code") final String filmCode, @RequestBody final String filmName){
        Film film = service.getFilmByCode(filmCode);
        film.setFilmName(filmName);
        service.updateFilm(filmCode, film);
        return film;
    }

    @DeleteMapping("/{film_code}")
    public Film deleteFilm(@PathVariable("film_code") final String filmCode){
        Film film = service.getFilmByCode(filmCode);
        service.deleteFilmByCode(filmCode);
        return film;
    }

    @PostMapping("/{film_code}")
    public ResponseEntity<List<Schedule>> addSchedule(@PathVariable("film_code") final String filmCode, @RequestBody final Schedule schedule){
        service.addSchedule(filmCode, schedule);
        return ResponseEntity.accepted().body(service.getSchedules(filmCode));
    }

}
