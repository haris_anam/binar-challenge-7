package com.binar.chapter4.model.dto;

import java.util.Set;

public class SignupRequest {
    private String username;
    private String password;

    private String email;

    private Set<String> roles;

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public String getEmail() {
        return email;
    }

    public Set<String> getRoles() {
        return roles;
    }
}
