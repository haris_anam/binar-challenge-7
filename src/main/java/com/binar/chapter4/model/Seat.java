package com.binar.chapter4.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Accessors(chain=true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "seats")
public class Seat implements Serializable {
    private static final long serialVersionUID = 4L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Embedded
    private SeatNumber seatNumber;
    @Enumerated(EnumType.ORDINAL)
    private StudioNames studioName;
    @ManyToOne(fetch = FetchType.LAZY)
    private Schedule schedule;
}

