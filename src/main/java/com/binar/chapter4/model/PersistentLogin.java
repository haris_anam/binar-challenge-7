package com.binar.chapter4.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "persistent_logins")
public class PersistentLogin {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;
    private String username;
    private String series;
    private Date last_used;
}
