package com.binar.chapter4.implementation;

import com.binar.chapter4.model.User;
import com.binar.chapter4.repository.UserRepository;
import com.binar.chapter4.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository repository;

    @Override
    public User addUser(User user) {
        return repository.save(user);
    }
    @Override
    public User getUserByUsername(String username) {
        return repository.findById(username).get();
    }
    @Override
    public List<User> getAllUsers(){
        return repository.findAll();
    }
    @Override
    public void updateUser(String username, User user) {
        User userDB = repository.findById(username).orElseThrow(null);
        repository.delete(userDB);
        repository.save(user);
    }
    @Override
    public void deleteUserByUsername(String username) {
        try {
            repository.deleteById(username);
        }catch(DataAccessException ex){
            throw new RuntimeException(ex);
        }
    }
}
