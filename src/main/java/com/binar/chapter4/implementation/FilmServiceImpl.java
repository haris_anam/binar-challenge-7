package com.binar.chapter4.implementation;

import com.binar.chapter4.model.Film;
import com.binar.chapter4.model.Schedule;
import com.binar.chapter4.repository.FilmRepository;
import com.binar.chapter4.repository.ScheduleRepository;
import com.binar.chapter4.service.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class FilmServiceImpl implements FilmService {
    @Autowired
    private ScheduleRepository scheduleRepository;
    @Autowired
    private FilmRepository repository;

    @Override
    public Film addFilm(Film film) {
        return repository.save(film);
    }
    @Override
    public Film getFilmByCode(String code) {
        return repository.findById(code).get();
    }
    @Override
    public List<Film> getAllFilms(){
        return repository.findAll();
    }

    @Override
    public void updateFilm(String code, Film film) {
        Film filmDB = repository.findById(code).get();
        filmDB.setFilmName(film.getFilmName());
        filmDB.setSchedules(film.getSchedules());
        repository.save(filmDB);
    }

    @Override
    public List<Film> getShowingFilms() {
        Timestamp now = new Timestamp(new Date().getTime());
        return repository.findBySchedules_JamMulaiIsLessThanAndSchedules_JamSelesaiIsGreaterThanAndSchedulesNotEmpty(now, now);
    }

    @Override
    public List<Schedule> getSchedules(String filmCode) {
        return scheduleRepository.findByFilm_FilmCodeEquals(filmCode);
    }

    @Override
    public Film addSchedule(String filmCode, Schedule schedule) {
        Film film = repository.findById(filmCode).get();
        schedule.setFilm(film);
        scheduleRepository.save(schedule);
        return repository.save(film);
    }

    @Transactional
    @Override
    public void deleteFilmByCode(String code) {
        scheduleRepository.deleteByFilm_FilmCodeEquals(code);
        repository.deleteById(code);
    }
}