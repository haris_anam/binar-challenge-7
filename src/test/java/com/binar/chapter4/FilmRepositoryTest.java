package com.binar.chapter4;

import com.binar.chapter4.implementation.FilmServiceImpl;
import com.binar.chapter4.model.Film;
import com.binar.chapter4.model.Schedule;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FilmRepositoryTest {
    @Autowired
    FilmServiceImpl filmService;

    static final String PRIMARY_FILM_CODE = "AHOA23";
    static final String PRIMARY_FILM_NAME = "Moana";
    static final String PRIMARY_UPDATED_NAME = "Moana 2";
    static final String SECONDARY_FILM_CODE = "HADF52";
    static final String SECONDARY_FILM_NAME = "Batman";

    @Before
    public void init() {
        System.out.println("Initiating...");
    }

    @Test
    public void addFilm() {
        Film appendedFilm = filmService.addFilm(new Film(PRIMARY_FILM_CODE, PRIMARY_FILM_NAME, new ArrayList<>()));

        Schedule schedule = createSchedule(0, 1);
        Schedule schedule2 = createSchedule(1, 2);

        filmService.addSchedule(appendedFilm.getFilmCode(), schedule);
        filmService.addSchedule(appendedFilm.getFilmCode(), schedule2);
        assertThat(appendedFilm).isNotNull();
    }

    @Test
    public void updateFilm(){
        filmService.updateFilm(PRIMARY_FILM_CODE, filmService.getFilmByCode(PRIMARY_FILM_CODE).setFilmName(PRIMARY_UPDATED_NAME));
        assertThat(filmService.getFilmByCode(PRIMARY_FILM_CODE).getFilmName()).isEqualTo(PRIMARY_UPDATED_NAME);
    }

    @Test
    public void getShowingFilms(){
        Film film = new Film(SECONDARY_FILM_CODE, SECONDARY_FILM_NAME, new ArrayList<>());

        Schedule schedule = createSchedule(-2, -1);

        Film appendedFilm = filmService.addFilm(film);
        filmService.addSchedule(appendedFilm.getFilmCode(), schedule);

        List<Film> showing = filmService.getShowingFilms();
        assertThat(showing.size()).isEqualTo(1);
    }

    @Test
    public void showSchedules(){
        List<Schedule> schedules = filmService.getSchedules(PRIMARY_FILM_CODE);
        assertThat(schedules.size()).isEqualTo(2);
    }

    @Test
    public void deleteFilm(){
        filmService.deleteFilmByCode(SECONDARY_FILM_CODE);
        assertThat(filmService.getAllFilms().size()).isEqualTo(1);
    }

    private Schedule createSchedule(int diff1, int diff2){
        Schedule schedule = new Schedule();
        schedule.setSeats(new ArrayList<>());

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.HOUR_OF_DAY, diff1);
        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(new Date());
        calendar2.add(Calendar.HOUR_OF_DAY, diff2);

        schedule.setTanggalTayang(new java.sql.Date(calendar.getTime().getTime()));
        schedule.setJamMulai(new Timestamp(calendar.getTime().getTime()));
        schedule.setJamSelesai(new Timestamp(calendar2.getTime().getTime()));
        return schedule;
    }

}
